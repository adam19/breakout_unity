﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillVolume : MonoBehaviour {

	public GameStateManager GameStateManagerRef;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void OnTriggerEnter(Collider other)
	{
		Ball BallComp = other.gameObject.GetComponent<Ball>();
		if (BallComp)
		{
			Debug.Log("Ball collided with me. Killing it!");
			Destroy(BallComp.gameObject);

			if (GameStateManagerRef)
			{
				GameStateManagerRef.OnPlayerDeath();
			}
		}
	}
}
