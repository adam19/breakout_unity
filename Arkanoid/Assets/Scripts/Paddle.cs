﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paddle : MonoBehaviour {

	public GameStateManager GameStateManagerRef;
	public Vector3 LeftBoundary;
	public Vector3 RightBoundary;
	public float MaxPaddleSpeed = 1.0f;
	public float EdgeDeflectionAngle = 45.0f;
	
	private GameObject _BallGameObjRef;
	private BallSpawner _PaddleBallSpawner;
	private Vector3 _HalfSize;

	public Vector3 HalfSize
	{
		get { return _HalfSize; }
	}


	private void Awake()
	{
		// Grab the size of the paddle collider
		Collider PaddleCollider = gameObject.GetComponent<Collider>();
		if (PaddleCollider)
		{
			_HalfSize = PaddleCollider.bounds.extents;
		}

		_PaddleBallSpawner = gameObject.GetComponent<BallSpawner>();
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void OnDrawGizmos()
	{
		Vector3 WireCubeSize = new Vector3(0, 1, 1);

		Gizmos.DrawCube(LeftBoundary + transform.position, WireCubeSize);
		Gizmos.DrawCube(RightBoundary + transform.position, WireCubeSize);
		Gizmos.DrawRay(LeftBoundary + transform.position, new Vector3(1, 0, 0));
		Gizmos.DrawRay(RightBoundary + transform.position, new Vector3(-1, 0, 0));
	}

	private void OnCollisionEnter(Collision collision)
	{
		Ball BallRef = collision.gameObject.GetComponent<Ball>();
		if (BallRef)
		{
			foreach(ContactPoint cp in collision.contacts)
			{
				// Ensure the collision occurred on the top of the paddle, and not the side
				if (Vector3.Dot(cp.normal, Vector3.up) < 0.0f)
				{
					Vector3 LocalContactPoint = cp.point - transform.position;

					// Get a scalar value [-1, 1] for the contact point on the paddle along the x-axis
					float DeflectionAmount = 0.0f;
					if (_HalfSize.x > 0.0f)
					{
						DeflectionAmount = LocalContactPoint.x / _HalfSize.x;                       // Scale to [-1, 1]
						DeflectionAmount = DeflectionAmount * EdgeDeflectionAngle * Mathf.Deg2Rad;  // Scale to EdgeDeflectionAngle
					}

					// Generate the DeflectionVector
					Vector3 DeflectionVector = new Vector3(DeflectionAmount, 1.0f, 0.0f).normalized;
					Vector3 BallDirection = BallRef.GetVelocity().normalized;
					float BallSpeed = BallRef.GetVelocity().magnitude;

					BallRef.SetVelocity(DeflectionVector * BallSpeed);

					//Debug.Log("DeflectionVector: " + DeflectionVector);
				}
			}

			// Reset the score multiplier
			GameStateManagerRef.ResetScoreMultiplier();
		}
	}

	public void HandleInput(Vector3 MovementAmount)
	{
		// Keep the paddle within the bounds of the playing field
		Vector3 NewPosition = transform.position;
		NewPosition.x += (MovementAmount.x * MaxPaddleSpeed * Time.deltaTime);
		NewPosition.x = Mathf.Clamp(NewPosition.x, LeftBoundary.x + _HalfSize.x, RightBoundary.x - _HalfSize.x);

		// Update the paddle position
		Vector3 PaddleOffset = NewPosition - transform.position;
		transform.position += PaddleOffset;
	}

	public void AttemptSpawnBall()
	{
		if (_PaddleBallSpawner && GameStateManager.GameState == GameStateManager.GameStateEnum.STARTING)
		{
			_BallGameObjRef = _PaddleBallSpawner.Spawn();
		}
	}

	public void AttemptBallLaunch()
	{
		if (_BallGameObjRef && GameStateManager.GameState == GameStateManager.GameStateEnum.PLAYING)
		{
			_PaddleBallSpawner.Launch(new Vector3(0, 1, 0));
		}
	}
}
