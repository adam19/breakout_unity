﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallSpawner : Spawner {
	
	public float NormalSpeed = 1.0f;

	private GameObject _BallRef;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public override GameObject Spawn()
	{
		_BallRef = base.Spawn();

		// Move the ball upwards so it's not colliding with the parent object
		if (_BallRef)
		{
			Collider BallCollider = _BallRef.GetComponent<Collider>();
			if (BallCollider)
			{
				// Offset the height of the ball so it's not inside the platform
				Vector3 SpawnPosition = GetSpawnPosition();
				SpawnPosition.y += BallCollider.bounds.extents.y;

				_BallRef.transform.position = SpawnPosition;
			}

			_BallRef.transform.parent = gameObject.transform;
		}

		return _BallRef;
	}

	public void Launch(Vector3 Direction)
	{
		// Only launch a ball if we have a reference to it. We can't launch anything if it's not attached to the spawner
		if (_BallRef)
		{
			Ball BallComp = _BallRef.GetComponent<Ball>();
			if (BallComp)
			{
				if (Direction.sqrMagnitude > 0.0f)
				{
					BallComp.SetVelocity(Direction.normalized * NormalSpeed);
				}

				// Disconnect the ball from the platform
				BallComp.gameObject.transform.parent = null;
				_BallRef = null;
			}
		}
	}
}
