﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextButton : MonoBehaviour {
	
	public delegate void ClickDelegate();

	public ClickDelegate OnClickDelegate;
	public Color NormalColor = Color.white;
	public Color MouseOverColor = Color.white;
	public Color MouseDownColor = Color.white;

	private TextMesh _TextMeshComp;
	private bool _IsLerpingColor = false;
	private float _ColorLerpTime = 0.5f;
	private float _ColorLerpStartTime = 0.0f;
	private Color _StartColor;
	private Color _TargetColor;

	// Use this for initialization
	void Start () {
		_TextMeshComp = GetComponent<TextMesh>();
	}
	
	// Update is called once per frame
	void Update () {
		
		if (_IsLerpingColor)
		{
			float t = 1.0f;
			if (_ColorLerpTime > 0.0f)
			{
				t = (Time.timeSinceLevelLoad - _ColorLerpStartTime) / _ColorLerpTime;
			}

			if (t >= 1.0f)
			{
				_IsLerpingColor = false;
				t = 1.0f;
			}

			SetTextColor(Color.Lerp(_StartColor, _TargetColor, t));
		}
	}

	private void OnMouseEnter()
	{
		StartColorLerp(MouseOverColor);
	}

	private void OnMouseExit()
	{
		StartColorLerp(NormalColor);
	}

	private void OnMouseDown()
	{
		StartColorLerp(MouseOverColor);

		if (_TextMeshComp)
		{
			if (OnClickDelegate != null)
			{
				OnClickDelegate();
			}
		}
	}

	private void StartColorLerp(Color TargetColor)
	{
		_IsLerpingColor = true;
		_ColorLerpStartTime = Time.timeSinceLevelLoad;
		_TargetColor = TargetColor;

		if (_TextMeshComp)
		{
			_StartColor = _TextMeshComp.color;
		}
	}

	protected void SetTextColor(Color NewColor)
	{
		if (_TextMeshComp)
		{
			_TextMeshComp.color = NewColor;
		}
	}
}
