﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStateManager : MonoBehaviour {

	public enum GameStateEnum
	{
		MAINMENU,
		HOWTOPLAYMENU,
		STARTING,
		PLAYING,
		GAMEOVER_LOST,
		GAMEOVER_WON
	}

	public int TimeUntilGameStart = 3;
	public int LivesToStartWith = 3;
	public float ScoreMultiplierIncrement = 0.1f;
	public float ScoreMultiplierCap = 0.0f;
	public BlockManager BlockLayoutToSpawn;
	public Camera MainCameraRef;
	public Transform CameraTransformMainMenu;
	public Transform CameraTransformHowToPlayMenu;
	public Transform CameraTransformGame;
	public float CameraSlerpTime = 1.0f;
	public TextMesh UIText;
	public TextMesh NumBallsText;
	public TextMesh ScoreText;
	public TextMesh ScoreMultiplierText;
	public TextButton MenuButton;

	public static GameStateEnum GameState
	{
		get { return _GameState; }
	}
	
	
	private static GameStateEnum _GameState;
	private static int _NumBalls;
	private static int _Score;
	private float _ScoreMultiplier = 1.0f;
	private PlayerController _PlayerControllerComp;
	private BlockManager _BlockManagerRef;
	private float _CountDownTime = -1.0f;

	// Camera-specific
	private Transform _TargetCameraTransform;
	private Transform _StartCameraTransform;
	private bool _IsCameraSlerping = false;
	private float _CameraSlerpStart = 0.0f;


	private bool IsDebugKeyDown = false;


	// Use this for initialization
	void Start () {

		_PlayerControllerComp = GetComponent<PlayerController>();

		SetCameraTargetTransform(CameraTransformMainMenu, true);
		OpenMainMenu();

		SetMenuButtonActive(false);
		if (MenuButton)
		{
			MenuButton.OnClickDelegate = OpenMainMenu;
		}
	}
	
	// Update is called once per frame
	void Update () {
		
		// Update the camera transform
		if (_IsCameraSlerping && MainCameraRef != null)
		{
			float t = Time.timeSinceLevelLoad - _CameraSlerpStart;
			if (CameraSlerpTime > 0.0f)
			{
				t /= CameraSlerpTime;
			}
			else
			{
				t = Mathf.Min(t, 1.0f);
			}

			// Slerp the rotation
			Quaternion CameraRotation = MainCameraRef.transform.rotation;
			CameraRotation = Quaternion.Slerp(_StartCameraTransform.rotation, _TargetCameraTransform.rotation, t);
			MainCameraRef.transform.rotation = CameraRotation;

			// Lerp the position
			Vector3 CameraPosition = MainCameraRef.transform.position;
			CameraPosition = Vector3.Lerp(_StartCameraTransform.position, _TargetCameraTransform.position, t);
			MainCameraRef.transform.position = CameraPosition;
		}

		// Display the countdown timer
		if (_CountDownTime > 0.0f)
		{
			int WholeSeconds = Mathf.CeilToInt(_CountDownTime);
			UIText.text = WholeSeconds.ToString();

			_CountDownTime -= Time.deltaTime;
		}
		else if (GameState == GameStateEnum.STARTING)
		{
			StartGame();
		}
	}

	private void SetCameraTargetTransform(Transform NewTargetTransform, bool InstantSnapToRotation = false)
	{
		if (NewTargetTransform != null)
		{
			_StartCameraTransform = MainCameraRef.transform;
			_TargetCameraTransform = NewTargetTransform;
			_IsCameraSlerping = !InstantSnapToRotation;

			if (MainCameraRef)
			{
				if (InstantSnapToRotation)
				{
					// Set all of the rotations to the new rotation
					_StartCameraTransform = NewTargetTransform;
					MainCameraRef.transform.SetPositionAndRotation(_StartCameraTransform.position, _StartCameraTransform.rotation);
				}
				else
				{
					_CameraSlerpStart = Time.timeSinceLevelLoad;
				}
			}
		}
	}

	public void OpenMainMenu()
	{
		_GameState = GameStateEnum.MAINMENU;
		Cursor.visible = true;

		SetTextActive(UIText, false);
		SetTextActive(NumBallsText, false);
		SetTextActive(ScoreText, false);
		SetTextActive(ScoreMultiplierText, false);
		SetMenuButtonActive(false);

		SetCameraTargetTransform(CameraTransformMainMenu);
	}

	public void OpenHowToPlayMenu()
	{
		_GameState = GameStateEnum.HOWTOPLAYMENU;
		Cursor.visible = true;

		SetTextActive(UIText, false);
		SetTextActive(NumBallsText, false);
		SetTextActive(ScoreText, false);
		SetTextActive(ScoreMultiplierText, false);

		SetCameraTargetTransform(CameraTransformHowToPlayMenu);
	}

	public void InitGame()
	{
		Debug.Log("InitGame()");

		_GameState = GameStateEnum.STARTING;
		Cursor.visible = false;

		if (_BlockManagerRef != null)
		{
			Destroy(_BlockManagerRef);
		}
		_BlockManagerRef = Instantiate(BlockLayoutToSpawn);
		if (_BlockManagerRef)
		{
			_BlockManagerRef.GameStateManagerRef = this;
		}

		_NumBalls = LivesToStartWith;
		_Score = 0;

		SetMenuButtonActive(false);
		SetTextActive(UIText, true);
		SetTextActive(NumBallsText, true);
		SetTextActive(ScoreText, true);
		SetTextActive(ScoreMultiplierText, true);
		UpdateBallsText();
		UpdateScoreText();
		UpdateMultiplierText();

		SetCameraTargetTransform(CameraTransformGame);
		StartGameAfterDelay(TimeUntilGameStart);
	}

	public void StartGame()
	{
		Debug.Log("StartGame()");

		_GameState = GameStateEnum.PLAYING;
		Cursor.visible = false;

		if (UIText)
		{
			UIText.text = "Go!";
			StartCoroutine(HideTextAfterDelay());
		}
	}

	public void QuitGame()
	{
		Application.Quit();
	}

	public void BlockDestroyed(Block DeadBlock)
	{
		if (DeadBlock != null)
		{
			_Score += (int)(DeadBlock.DestructionPoints * _ScoreMultiplier);

			UpdateScoreText();
			IncrementScoreMultiplier();
		}
	}

	public void AllBlocksDestroyed()
	{
		_GameState = GameStateEnum.GAMEOVER_WON;

		UIText.text = "You Won!";
		SetTextActive(UIText, true);

		SetMenuButtonActive(true);
	}

	public void ResetScoreMultiplier()
	{
		_ScoreMultiplier = 1.0f;
		UpdateMultiplierText();
	}

	public void IncrementScoreMultiplier()
	{
		_ScoreMultiplier += ScoreMultiplierIncrement;
		_ScoreMultiplier = Mathf.Max(_ScoreMultiplier, ScoreMultiplierCap);
		UpdateMultiplierText();
	}

	public void OnPlayerDeath()
	{
		if (_GameState != GameStateEnum.GAMEOVER_WON)
		{
			if (_NumBalls == 0)
			{
				_GameState = GameStateEnum.GAMEOVER_LOST;

				UIText.text = "Game Over!";
				SetMenuButtonActive(true);
			}
			else
			{
				_NumBalls--;
				UIText.text = "Ouch!";
				UpdateBallsText();
				StartGameAfterDelay(TimeUntilGameStart);
			}

			ResetScoreMultiplier();
			SetTextActive(UIText, true);
		}
	}

	private void StartGameAfterDelay(int Seconds)
	{
		_GameState = GameStateEnum.STARTING;
		_CountDownTime = Seconds;
		
		if (_PlayerControllerComp)
		{
			_PlayerControllerComp.SpawnBall();
		}
	}

	private IEnumerator HideTextAfterDelay()
	{
		yield return new WaitForSeconds(2);

		SetTextActive(UIText, false);
	}

	private void SetMenuButtonActive(bool IsActive)
	{
		if (IsActive)
		{
			Cursor.visible = true;
		}

		if (MenuButton)
		{
			MenuButton.gameObject.SetActive(IsActive);
		}
	}

	private void SetTextActive(TextMesh TextObj, bool IsActive)
	{
		if (TextObj)
		{
			TextObj.gameObject.SetActive(IsActive);
		}
	}

	private void UpdateBallsText()
	{
		if (NumBallsText)
		{
			NumBallsText.text = "Balls: " + _NumBalls;
		}
	}

	private void UpdateScoreText()
	{
		if (ScoreText)
		{
			ScoreText.text = "Score: " + _Score;
		}
	}

	private void UpdateMultiplierText()
	{
		if (ScoreMultiplierText)
		{
			ScoreMultiplierText.text = string.Format("x{0:N1}", _ScoreMultiplier);
		}
	}
}
