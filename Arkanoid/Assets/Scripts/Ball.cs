﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour {

	public Vector3 Velocity;

	private ParticleSystem _HighSpeedParticleEffect;


	// Use this for initialization
	void Start () {
		
		_HighSpeedParticleEffect = GetComponent<ParticleSystem>();
		if (_HighSpeedParticleEffect)
		{
			_HighSpeedParticleEffect.Stop(true, ParticleSystemStopBehavior.StopEmitting);
		}
	}
	
	// Update is called once per frame
	void Update () {

	}

	public void EnableHighSpeedEffect()
	{
		if (_HighSpeedParticleEffect)
		{
			_HighSpeedParticleEffect.Play(true);
		}
	}

	public void DisableHighSpeedEffect()
	{
		if (_HighSpeedParticleEffect)
		{
			_HighSpeedParticleEffect.Stop(true, ParticleSystemStopBehavior.StopEmitting);
		}
	}

	public Vector3 GetVelocity()
	{
		Rigidbody RB = GetComponent<Rigidbody>();
		if (RB)
		{
			return RB.velocity;
		}
		else
		{
			return this.Velocity;
		}
	}

	public void SetVelocity(Vector3 Velocity)
	{
		Rigidbody RB = GetComponent<Rigidbody>();
		if (RB)
		{
			RB.velocity = Velocity;
		}
		else
		{
			this.Velocity = Velocity;
		}

		if (this.Velocity.sqrMagnitude > 0.0f)
		{
			EnableHighSpeedEffect();
		}
		else
		{
			DisableHighSpeedEffect();
		}
	}
}
