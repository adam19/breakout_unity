﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	public GameObject PaddleToSpawn;
	public Transform PaddleSpawnLocation;

	private GameStateManager _GameStateManagerRef;
	private Vector2 _CurrentMousePosition;
	private Vector2 _PreviousMousePosition;

	private GameObject _PaddleGameObjRef;
	private Paddle _PaddleCompRef;
	

	// Use this for initialization
	void Start () {
		
		_PreviousMousePosition = _CurrentMousePosition = Input.mousePosition;
		_GameStateManagerRef = GetComponent<GameStateManager>();

		SpawnPaddle();
	}
	
	// Update is called once per frame
	void Update () {

		if (_GameStateManagerRef != null)
		{
			if (GameStateManager.GameState == GameStateManager.GameStateEnum.PLAYING)
			{
				_PreviousMousePosition = _CurrentMousePosition;
				_CurrentMousePosition = Input.mousePosition;

				Vector3 MouseDif = _CurrentMousePosition - _PreviousMousePosition;
				if (_PaddleCompRef)
				{
					_PaddleCompRef.HandleInput(MouseDif);

					// If the ball hasn't been launched, check if the player wants to launch it
					if (Input.GetMouseButtonDown(0))
					{
						_PaddleCompRef.AttemptBallLaunch();
					}
				}
			}
		}

	}

	void SpawnPaddle()
	{
		if (PaddleToSpawn)
		{
			_PaddleGameObjRef = Instantiate(PaddleToSpawn);
			if (_PaddleGameObjRef)
			{
				_PaddleGameObjRef.transform.SetPositionAndRotation(PaddleSpawnLocation.position, PaddleSpawnLocation.rotation);

				// Update the spawned Paddle with the proper GameStateManager
				_PaddleCompRef = _PaddleGameObjRef.GetComponent<Paddle>();
				if (_PaddleCompRef)
				{
					_PaddleCompRef.GameStateManagerRef = _GameStateManagerRef;
				}
			}
		}
	}

	public void SpawnBall()
	{
		if (_PaddleCompRef)
		{
			_PaddleCompRef.AttemptSpawnBall();
		}
	}
}
