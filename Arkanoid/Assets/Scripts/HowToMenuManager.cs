﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HowToMenuManager : MenuManager {

	public TextButton ReturnButton;

	// Use this for initialization
	void Start () {

		if (GameStateManagerRef != null)
		{
			ReturnButton.OnClickDelegate = GameStateManagerRef.OpenMainMenu;
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
