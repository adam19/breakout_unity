﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

	public GameObject PrefabToSpawn;
	public Vector3 Offset = new Vector3();
	public float DebugGizmoRadius = 1.0f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public virtual GameObject Spawn()
	{
		if (PrefabToSpawn)
		{
			return Instantiate(PrefabToSpawn, GetSpawnPosition(), Quaternion.identity);
		}

		return null;
	}

	protected Vector3 GetSpawnPosition()
	{
		return transform.position + Offset;
	}

	private void OnDrawGizmos()
	{
		Gizmos.DrawWireSphere(transform.position + Offset, DebugGizmoRadius);
	}
}
