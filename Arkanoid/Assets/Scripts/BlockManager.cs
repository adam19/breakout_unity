﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockManager : MonoBehaviour {

	public GameStateManager GameStateManagerRef;

	private Block[] _ChildBlocks;
	private int _TotalBlocksAlive;


	// Use this for initialization
	void Start () {
		_ChildBlocks = GetComponentsInChildren<Block>();
		_TotalBlocksAlive = _ChildBlocks.Length;

		for (int i=0; i<_ChildBlocks.Length; i++)
		{
			_ChildBlocks[i].SetBlockId(i);
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OnBlockDeath(Block DeadBlock)
	{
		if (DeadBlock != null && DeadBlock.BlockId > -1)
		{
			_TotalBlocksAlive--;

			if (GameStateManagerRef)
			{
				if (_TotalBlocksAlive <= 0)
				{
					GameStateManagerRef.AllBlocksDestroyed();
				}
				else
				{
					GameStateManagerRef.BlockDestroyed(DeadBlock);
				}
			}
		}
	}

	public void DebugKillABlock()
	{
		for (int i=0; i<_ChildBlocks.Length; i++)
		{
			if (_ChildBlocks[i].IsAlive)
			{
				_ChildBlocks[i].OnDeath();
				break;
			}
		}
	}
}
