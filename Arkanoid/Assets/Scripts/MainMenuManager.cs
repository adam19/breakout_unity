﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuManager : MenuManager {

	public TextButton StartGameButton;
	public TextButton HowToPlayButton;
	public TextButton QuitGameButton;

	// Use this for initialization
	void Start () {

		if (GameStateManagerRef != null)
		{
			StartGameButton.OnClickDelegate = GameStateManagerRef.InitGame;
			HowToPlayButton.OnClickDelegate = GameStateManagerRef.OpenHowToPlayMenu;
			QuitGameButton.OnClickDelegate = GameStateManagerRef.QuitGame;
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
