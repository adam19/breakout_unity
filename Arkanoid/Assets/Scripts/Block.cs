﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : MonoBehaviour {

	public ParticleSystem DestructionEffect;
	public int DestructionPoints = 100;

	[HideInInspector]
	public int BlockId = -1;

	private BlockManager _BlockManagerComp;
	private MeshRenderer _MeshRenderer;
	private BoxCollider _BoxCollider;
	private bool _IsAlive = true;

	public bool IsAlive
	{
		get { return _IsAlive; }
	}

	// Use this for initialization
	void Start () {

		DestructionEffect = GetComponent<ParticleSystem>();

		_BlockManagerComp = GetComponentInParent<BlockManager>();
		_MeshRenderer = GetComponent<MeshRenderer>();
		_BoxCollider = GetComponent<BoxCollider>();
	}
	
	// Update is called once per frame
	void Update () {

	}

	private void OnCollisionEnter(Collision collision)
	{
		Ball BallComp = collision.gameObject.GetComponent<Ball>();
		if (BallComp)
		{
			OnDeath();
		}
	}

	public void SetBlockId(int NewId)
	{
		BlockId = NewId;
	}

	public void OnDeath()
	{
		if (_IsAlive)
		{
			// Kick off particle effect
			float DelayToDestroy = 1.0f;

			if (DestructionEffect && !DestructionEffect.isPlaying)
			{
				if (DestructionEffect.main.startLifetime.curveMax != null)
				{
					AnimationCurve LifetimeCurve = DestructionEffect.main.startLifetime.curveMax;
					DelayToDestroy = LifetimeCurve.keys[LifetimeCurve.length - 1].value;
				}
				else
				{
					DelayToDestroy = DestructionEffect.main.startLifetime.constantMax;
				}

				// Play and detach the effect from the block
				DestructionEffect.Play(true);
			}

			// Hide the block & disable the collider
			_MeshRenderer.enabled = false;
			_BoxCollider.enabled = false;
			_IsAlive = false;

			// Update the BlockManager
			if (_BlockManagerComp)
			{
				_BlockManagerComp.OnBlockDeath(this);
			}
		}
	}
}
